#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
typedef struct{
    char *key;
    int value;
}Map;
int checkNum(char* str, void* value){
    if(sscanf(str,"%d", (int*)value)!=1){
        return -1;
    }
    return 0;
}

int checkKey(char* str, void* value){
    if(sscanf(str,"%s", (char*)value)!=1){
        return -1;
    }

    return 0;
}
int checkValue(char* str, void* value){
    if(sscanf(str,"%d", (int*)value)!=1){
        return -1;
    }

    return 0;
}
int checkInput(char *str,void *value){
    Map* map = (Map*) value;
	char * e = str;
    e = strchr(e, '=');
    if(!e) return -1;
    *e='\0';
    e++;
    if(!map->key)
        map->key = (char*) calloc(strlen(str)+2, sizeof(char));
    else
        map->key = (char*) realloc(map->key, (strlen(str)+2)*sizeof(char));
	if (checkKey(str, map->key))
		return -2;
    if(sscanf(e,"%d",&map->value)!=1)
		return -3;
    return 0;

}
int readData(char* msg, int (*check)(char*, void*), void* value, int hasNull){
    int val;
    int res = 0;
    int err;
    char* str = (char*) calloc(128, sizeof(char));
    printf("%s\n", msg);
    fflush(stdin);
//    __fpurge(stdin);
    while(1){
        //printf("Введите информацию в формате: КЛЮЧ=ЗНАЧЕНИЕ : ");
        fgets(str, 128, stdin);
        if(hasNull)
            if(str[0]=='\n'){
                res = 1;
                break;
            }

        if(err = check(str, value)){
            printf("Ошибка ввода, повторите ввод (%d)\n", err);
            }else{
                break;
        }
    }
    free(str);
    return res;
}
Map* Input(){
    Map * map;
    map=(Map*)malloc(sizeof(Map));
    map->key=NULL;
    if(readData("Введите информацию в формате: КЛЮЧ=ЗНАЧЕНИЕ :",checkInput,map, 1)){
        free(map);
        return NULL;
    }
    return map;
}
void printMap(Map ** maps, int size){
    int i;
    for(i=0;i<size;i++)
        printf("%s=%d \n",maps[i]->key,maps[i]->value);
}
int main(int argc, char *argv[])
{
    system("chcp 65001");
    int i=0,val;
    char str[100];
    int N=0;
    int NN=1;
    Map ** maps = calloc(NN, sizeof(Map**));

    while(1){
        if(N>=NN)
            maps = realloc(maps, (NN+=10)*sizeof(Map**));
        maps[N]=Input();
        if(maps[N]==NULL)
            break;
        N++;
    }
    printMap(maps,N);
    maps = realloc(maps, N*sizeof(Map**));
    int n;
    readData("Введите N: ", checkNum, &n, 0);
    printf("Пребразованные данные: \n");
    for(i=0;i<N;i++){
    if(maps[i]->value!=n)
        printf("%s=%d \n",maps[i]->key,maps[i]->value);

    }

    printf("Hello world!\n");

    return 0;
}
